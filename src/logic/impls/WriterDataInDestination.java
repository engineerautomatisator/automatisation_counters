package logic.impls;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Workbook;

import logic.interfaces.ICounter;
import logic.interfaces.IDestination;
import logic.interfaces.IHandlerRow; 
import logic.interfaces.IWriterDataInDestination;
/**
 * ����� ��� ������ ������ � ����
 * @author �������
 *
 */
public class WriterDataInDestination implements IWriterDataInDestination {

	private static CellStyle createBorderedStyle(Workbook wb) {
		CellStyle style = wb.createCellStyle();
		style.setBorderRight(CellStyle.BORDER_THIN);
		style.setRightBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderBottom(CellStyle.BORDER_THIN);
		style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderLeft(CellStyle.BORDER_THIN);
		style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderTop(CellStyle.BORDER_THIN);
		style.setTopBorderColor(IndexedColors.BLACK.getIndex());
		return style;
	}
	private void writeFile(List<ICounter> counters, IDestination destination)throws Exception{
		
		HSSFWorkbook wb = new HSSFWorkbook(new FileInputStream(destination.getFullPath()));
		CellStyle style6 = createBorderedStyle(wb);
		style6.setFillForegroundColor(IndexedColors.ROSE.getIndex());
		style6.setFillPattern(CellStyle.SOLID_FOREGROUND);

		CellStyle style7 = createBorderedStyle(wb);
		style7.setFillForegroundColor(IndexedColors.AQUA.getIndex());
		style7.setFillPattern(CellStyle.SOLID_FOREGROUND);
		
		IHandlerRow handlerRow = new HandlerRowImpl.HandlerRowImplBuilder()
				.setDangerStyle(style6)
				.setSuccessStyle(style7)
				.build();
		
		HSSFSheet sheet = wb.getSheetAt(0);
		HSSFRow row;

		int rows; // No of rows
		rows = sheet.getPhysicalNumberOfRows();
		 
		int founded = 0; 
		for (int i = destination.getStartRow(); 
				i < destination.getEndRow() && i < rows;
				i++) {
			row = sheet.getRow(i);
			founded += handlerRow.handleRow(row, sheet, i, counters);
		 
		}
		FileOutputStream outputStream = new FileOutputStream(new File(destination.getFullPathForSave()));
		wb.write(outputStream);
		outputStream.close();// Close in finally if possible
	}
	
	@Override
	public void write(List<ICounter> counters, List<IDestination> destinations) throws Exception{
		for (IDestination destination : destinations){
			writeFile(counters, destination);
		}
		
	}
	
}
