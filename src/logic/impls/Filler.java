package logic.impls;

import java.util.List;

import logic.interfaces.ICounter;
import logic.interfaces.IFiller;

public class Filler implements IFiller {
	
	@Override
	public void insert(List<ICounter> counters, ICounter pretendent) {
		 for (ICounter iCounter : counters){
			 if (HelperAll.isIdentityCounters(iCounter, pretendent)){
				 return;
			 }
		 }
		 counters.add(pretendent);
	}
	

}
