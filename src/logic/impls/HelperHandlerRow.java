package logic.impls;

import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;


import logic.interfaces.ICounter;
import logic.interfaces.IHelperHandlerRow;

/**
 * ����� �������� ��� ��������� ������
 * @author �������
 *
 */
public class HelperHandlerRow implements IHelperHandlerRow {

	
	
	@Override
	public boolean trySetValueData(HSSFRow row, double num, CellStyle style) {
		HSSFCell cell = row.getCell(6);
		if (cell == null){
			return false;
		}
		cell.setCellValue(num);
		cell.setCellStyle(style);
		return true;
	}

	@Override
	public String getNumUcheta(HSSFRow row) {
		HSSFCell cell = row.getCell(3);
		String str = null; 
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_NUMERIC: {
			str = String.valueOf(cell.getNumericCellValue());
			break;
		}
		case Cell.CELL_TYPE_STRING:
			str = cell.getStringCellValue();
			break;
		}
		return str;
	}

	@Override
	public double getTarif(HSSFRow row) {
		HSSFCell cell = row.getCell(4);
		final String DAY = "����"; 
 
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_NUMERIC: {
			return cell.getNumericCellValue();
		}
		case Cell.CELL_TYPE_STRING:
				return  cell.getStringCellValue().equals(DAY) ? 1 : 2;
		}
		return 0;
	}

	@Override
	public double getDataByString(List<ICounter> sourcesList, String numUcheta, double tarif) {
		for (ICounter dataSource : sourcesList) {
			// System.out.println(dataSources.code +" == " + tarif + " && " +
			// dataSources.serialNum + " == " + numUcheta);
			if (dataSource.getTariffZone() == tarif &&  
					HelperAll.isApropriateNumberScetchik(dataSource, numUcheta)){
				dataSource.onFound();
				return dataSource.getIndcations();
			}

		}
		return 0;
	}

}
