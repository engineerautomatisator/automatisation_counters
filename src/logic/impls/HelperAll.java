package logic.impls;

import logic.interfaces.ICounter;

public class HelperAll {
	public static boolean isApropriateNumberScetchik(ICounter counter, String numUcheta){
		if (counter.getFactoryNumberStr() != null){
			return numUcheta.equals(counter.getFactoryNumberStr());
		}
		try{
		
			double value = Double.valueOf(numUcheta); 
			return Double.compare(value, counter.getFactoryNumberNum()) == 0;
		}catch(Exception e){
			
		}
		return numUcheta.equals(String.valueOf(counter.getFactoryNumberNum()));
		
	}
	 
	
	public static boolean isIdentityCounters(ICounter counter, ICounter pretendent){
		boolean isEqualTariffZone = Double.compare(pretendent.getTariffZone(), counter.getTariffZone()) == 0;
		if (!isEqualTariffZone){
			return false;
		}
		boolean isEqualNumber = false;
		if (counter.getFactoryNumberStr() != null && pretendent.getFactoryNumberStr() != null){
			isEqualNumber =  counter.getFactoryNumberStr().equals(pretendent.getFactoryNumberStr());
		}else if (counter.getFactoryNumberStr() == null && pretendent.getFactoryNumberStr() != null){
			double predVal = 0;
			try{
				 predVal = Double.valueOf(pretendent.getFactoryNumberStr());
			}catch(Exception e){
				return false;
			}
			
			isEqualNumber = Double.compare(counter.getFactoryNumberNum(), predVal) == 0;
		}else if (counter.getFactoryNumberStr() != null && pretendent.getFactoryNumberStr() == null){
			double predVal = 0;
			try{
				predVal = Double.valueOf(counter.getFactoryNumberStr());
			}catch(Exception e){
				return false;
			}
			isEqualNumber = Double.compare(pretendent.getFactoryNumberNum(), predVal) == 0;
		}else {
			double counterVal = counter.getFactoryNumberNum();
			double predVal = pretendent.getFactoryNumberNum();
			isEqualNumber = Double.compare(counterVal, predVal) == 0;
		}
		return isEqualNumber && isEqualTariffZone;
	}
}
