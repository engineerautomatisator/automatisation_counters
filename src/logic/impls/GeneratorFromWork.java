package logic.impls;

import java.io.FileInputStream;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;

import logic.impls.Counter.CounterBuilder;
import logic.interfaces.ICounter;
import logic.interfaces.IFiller;
import logic.interfaces.IGeneratorCounter;

public class GeneratorFromWork implements IGeneratorCounter {

	private String m_fileName;
	private Integer m_endRow;
	private Integer m_startRow; 
	public GeneratorFromWork(final String fileName, Integer startRow, Integer endRow){
		m_fileName = fileName;
		m_endRow = endRow;
		m_startRow = startRow;
	}
	private Double getVal(HSSFCell cell, double def){
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_NUMERIC:
			return cell.getNumericCellValue();
		case Cell.CELL_TYPE_STRING:
			return Double.parseDouble(cell.getStringCellValue());
		default:
			break;
		}  
		return def;
	}
	private double getTarifZoneFromString(final String zoneTarif){
		if ("����".equals(zoneTarif)){
			return 1;
		}
		return 2;
			
	}
	@Override
	public void generate(List<ICounter> counters, IFiller iFiller) throws Exception {
	 
			// Get the workbook instance for XLSX file
			HSSFWorkbook wb = new HSSFWorkbook(new FileInputStream(m_fileName));

			// Get first sheet from the workbook
			HSSFSheet sheet = wb.getSheetAt(0);
			HSSFRow row;  
			int rows; // No of rows
			rows = sheet.getPhysicalNumberOfRows();
		 
			System.out.println("test");
			for (int i = m_startRow; i < m_endRow && i <= rows; i++) {
				row = sheet.getRow(i);
				if (row != null) {
					int tmp = sheet.getRow(i).getPhysicalNumberOfCells();
					 if (tmp < 8){
						 continue;
					 }
					CounterBuilder counterBuilder = new Counter.CounterBuilder(); 
					
				 
					final String tariffZone = row.getCell(4).getStringCellValue();
					HSSFCell cell = row.getCell(3);
					
					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_NUMERIC:
						counterBuilder.setFactoryNumberNum(cell.getNumericCellValue()); 
						break;
					case Cell.CELL_TYPE_STRING:
						counterBuilder.setFactoryNumberStr(cell.getStringCellValue()); 
						break;
					default:
						break;
					}  
					
					HSSFCell cellIndicationsDay = row.getCell(6);
					Double pokazanya = getVal(cellIndicationsDay, 0);
					if (Math.abs(pokazanya) < 2 * Double.MIN_VALUE){
						continue;
					}
					counterBuilder.setIndications(pokazanya);
				 
					counterBuilder.setTarrifZone(getTarifZoneFromString(tariffZone));
					
					iFiller.insert(counters, counterBuilder.build());
				}
			}
			wb.close();
	 
	}

}
