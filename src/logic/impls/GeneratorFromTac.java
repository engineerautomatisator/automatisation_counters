package logic.impls;

import java.io.FileInputStream;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import logic.impls.Counter.CounterBuilder;
import logic.interfaces.ICounter;
import logic.interfaces.IFiller;
import logic.interfaces.IGeneratorCounter;

/**
 * ���������  ������ �� ����� � ���
 * @author �������
 *
 */
public class GeneratorFromTac implements IGeneratorCounter {
	
	private String m_fileName;
	private Integer m_endRow;
	final static int START_FROM_TAC = 5;
	
	public GeneratorFromTac(String file, Integer endRow){
		m_fileName = file;
		m_endRow = endRow;
	}
/*
	
	*/

	@Override
	public void generate(List<ICounter> counters, IFiller iFiller) {
		try {
			// Get the workbook instance for XLSX file
			XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(m_fileName));

			// Get first sheet from the workbook
			XSSFSheet sheet = wb.getSheetAt(0);
			XSSFRow row;
			XSSFCell cell;
			int rows; // No of rows
			rows = sheet.getPhysicalNumberOfRows();
			int cols = 0; // No of columns
			int tmp = 0;
			System.out.println(rows);
			int numers = 0;
			System.out.println("test");
			for (int i = START_FROM_TAC; i < m_endRow && i <= rows; i++) {
				row = sheet.getRow(i);
				if (row != null) {
					tmp = sheet.getRow(i).getPhysicalNumberOfCells();
					++numers;
					System.out.println(numers);
					CounterBuilder counterBuilder = new Counter.CounterBuilder();
					for (int j = 0; j < 6 || j < tmp; ++j) {
						cell = row.getCell(j);
						if (cell != null) {
							switch (cell.getCellType()) {

							case Cell.CELL_TYPE_BOOLEAN:
								// System.out.println(cell.getBooleanCellValue()
								// + " boolean");
								break;

							case Cell.CELL_TYPE_NUMERIC: {
								switch (j) {
								case 0:
									//dataS.reestr = cell.getNumericCellValue();
									break;
								case 2:
									counterBuilder.setFactoryNumberNum( cell.getNumericCellValue());
								 
									break;
								case 3:
									break;
								case 4:
									counterBuilder.setIndications(cell.getNumericCellValue());
									break;
								case 5:
									counterBuilder.setTarrifZone(cell.getNumericCellValue());
									break;
								}
								// System.out.println(cell.getNumericCellValue()+
								// " numeric");
								break;

							}
							case Cell.CELL_TYPE_STRING:
								//dataS.name = cell.getStringCellValue();
								// System.out.println(cell.getStringCellValue()+
								// " string");
								break;

							case Cell.CELL_TYPE_BLANK:
								// System.out.println(" ");
								break;

							default:
								System.out.println(cell);

							}
						}
					}
					iFiller.insert(counters, counterBuilder.build());
				}
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
	}

}
