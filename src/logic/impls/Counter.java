package logic.impls;

import logic.interfaces.ICounter; 
/**
 * 
 * ������ - ������� �������������� ��� ������ ��������� ��������
 * @author �������
 *
 */
public class Counter implements ICounter {

	private Double m_tariffZone;
	private String m_factoryNumber;
	private double m_indcations;
	private double m_factoryNumberNum;
	private boolean m_isFound;
	
	private Counter (CounterBuilder counterBuilder){
		m_tariffZone = counterBuilder._tariffZone;
		m_factoryNumber = counterBuilder._factoryNumber;
		m_indcations = counterBuilder._indcations;
		m_factoryNumberNum = counterBuilder._factoryNumberNum;
		m_isFound = false;
	}
	
	public static class CounterBuilder{
		private Double _tariffZone;
		private String _factoryNumber;
		private double _indcations;
		private double _factoryNumberNum;
		public void setTarrifZone(Double tariffZone){
			_tariffZone = tariffZone;
		}
		public CounterBuilder setFactoryNumberStr(String factoryNumber){
			_factoryNumber = factoryNumber;
			return this;
		}
		public CounterBuilder setFactoryNumberNum(double factoryNumberNum){
			_factoryNumberNum = factoryNumberNum;
			return this;
		}
		public CounterBuilder setIndications(double indications){
			_indcations = indications;
			return this;
		}
		public ICounter build(){
			return new Counter(this);
		}
	}


	@Override
	public String getFactoryNumberStr() { 
		return m_factoryNumber;
	}
	
	@Override
	public double getFactoryNumberNum() { 
		return m_factoryNumberNum;
	}


	@Override
	public Double getTariffZone() { 
		return m_tariffZone;
	}


	@Override
	public double getIndcations() { 
		return m_indcations;
	}

	@Override
	public boolean isFound() { 
		return m_isFound;
	}

	@Override
	public void onFound() {
		m_isFound = false;
	}
}
