package logic.impls;

import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.CellStyle;

import logic.interfaces.ICounter;
import logic.interfaces.IHandlerRow;
import logic.interfaces.IHelperHandlerRow;

/**
 * ����� ��� ��������� ������
 * @author �������
 *
 */
public class HandlerRowImpl implements IHandlerRow {
	
	private IHelperHandlerRow m_helperHandlerRow; 
	private CellStyle m_styleDanger;
	private CellStyle m_styleSucces;
	private HandlerRowImpl(HandlerRowImplBuilder builder){
		m_helperHandlerRow = builder.b_helperHandlerRow;
		m_styleSucces = builder.b_styleSucces;
		m_styleDanger = builder.b_styleDanger;
	}
	public static class HandlerRowImplBuilder{
		private IHelperHandlerRow b_helperHandlerRow;
		private CellStyle b_styleDanger;
		private CellStyle b_styleSucces;

		public HandlerRowImplBuilder(){
			b_helperHandlerRow = new HelperHandlerRow();
		}
		public HandlerRowImplBuilder setHelperHandlerRow(IHelperHandlerRow helperHandlerRow){
			b_helperHandlerRow = helperHandlerRow;
			return this;
		}
		public HandlerRowImplBuilder setDangerStyle(CellStyle danger){
			b_styleDanger = danger;
			return this;
		}
		public HandlerRowImplBuilder setSuccessStyle(CellStyle success){
			b_styleSucces = success;
			return this;
		}
		public IHandlerRow build(){
			return new HandlerRowImpl(this);
		}
	}
	@Override
	public Integer handleRow(HSSFRow row, HSSFSheet sheet, Integer index, List<ICounter> counters) {
	
		HSSFCell cell;
		int tmp = 0;
		int success = 0;
		if (row != null) {
			tmp = sheet.getRow(index).getPhysicalNumberOfCells();
			if (tmp < 7 )
				return 0;
			
			System.out.println(index);
			double tarif = -1;
			double numUcheta = -1;
			
			HSSFCell ncell = null;
			// System.out.println("test");
			if (!m_helperHandlerRow.trySetValueData(row, 0.0, m_styleDanger)){
				return 0;
			}
			String numberScetchik = m_helperHandlerRow.getNumUcheta(row);
			tarif = m_helperHandlerRow.getTarif(row);
			double value = m_helperHandlerRow.getDataByString(counters, numberScetchik, tarif);
			if (value != 0.0){
				if (!m_helperHandlerRow.trySetValueData(row, value, m_styleSucces)){
					return 0;
				}
			}
		}
		return success;
	}

}
