package logic.impls;

import logic.interfaces.IDestination;

/**
 * ����� �������������� ���� ��� ���������� �����������
 * @author �������
 *
 */
public class Destination implements IDestination{
	private String m_fullPath;
	private Integer m_startRow;
	private Integer m_endRow;
	private String m_pathForSave;
	
	private Destination(BuilderDestination builderDestination){
		m_fullPath    = builderDestination._fullPath;
		m_startRow    = builderDestination._startRow;
		m_endRow      = builderDestination._endRow;
		m_pathForSave = builderDestination._pathForSave;
	}
	
	public static final class BuilderDestination{
		private String  _fullPath;
		private String _pathForSave;
		private Integer _startRow;
		private Integer _endRow;
		public BuilderDestination(){
			_endRow = 11;
		}
		
		public BuilderDestination setFullPath(String fullPath){
			_fullPath = fullPath;
			return this;
		}
		
		public BuilderDestination setStartRow(Integer startRow){
			_startRow = startRow;
			return this;
		}
		public BuilderDestination setEndRow(Integer endRow){
			_endRow = endRow;
			return this;
		}
		public BuilderDestination setPathForSave(String pathForSave){
			_pathForSave = pathForSave;
			return this;
		}
		public IDestination build(){
			return new Destination(this);
		}
		
	}
	@Override
	public String getFullPath() { 
		return m_fullPath;
	}

	@Override
	public Integer getStartRow() { 
		return m_startRow;
	}

	@Override
	public Integer getEndRow() { 
		return m_endRow;
	}
	
	@Override
	public String getFullPathForSave(){
		return m_pathForSave;
	}

}
