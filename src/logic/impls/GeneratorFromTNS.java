package logic.impls;

import java.io.FileInputStream;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.examples.CellTypes;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import logic.impls.Counter.CounterBuilder;
import logic.interfaces.ICounter;
import logic.interfaces.IFiller;
import logic.interfaces.IGeneratorCounter;

public class GeneratorFromTNS implements IGeneratorCounter {
	
	private String m_fileName;
	private Integer m_endRow;
	final static int START_FROM_TNS = 2;
	public GeneratorFromTNS(final String fileName, Integer endRow){
		m_fileName = fileName;
		m_endRow = endRow;
	}
	private Double getVal(HSSFCell cell, double def){
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_NUMERIC:
			return cell.getNumericCellValue();
		case Cell.CELL_TYPE_STRING:
			return Double.parseDouble(cell.getStringCellValue());
		default:
			break;
		}  
		return def;
	}
	@Override
	public void generate(List<ICounter> counters, IFiller iFiller) throws Exception {
	 
			// Get the workbook instance for XLSX file
			HSSFWorkbook wb = new HSSFWorkbook(new FileInputStream(m_fileName));

			// Get first sheet from the workbook
			HSSFSheet sheet = wb.getSheetAt(0);
			HSSFRow row;  
			int rows; // No of rows
			rows = sheet.getPhysicalNumberOfRows();
		 
			System.out.println("test");
			for (int i = START_FROM_TNS; i < m_endRow && i <= rows; i++) {
				row = sheet.getRow(i);
				if (row != null) {
					int tmp = sheet.getRow(i).getPhysicalNumberOfCells();
					 if (tmp < 7){
						 continue;
					 }
					CounterBuilder counterBuilder = new Counter.CounterBuilder();
					CounterBuilder counterBuilderSecond = new Counter.CounterBuilder();
					boolean isBalnkNight = false;
					HSSFCell cell = row.getCell(1);
					
					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_NUMERIC:
						counterBuilder.setFactoryNumberNum(cell.getNumericCellValue());
						counterBuilderSecond.setFactoryNumberNum(cell.getNumericCellValue());
						break;
					case Cell.CELL_TYPE_STRING:
						counterBuilder.setFactoryNumberStr(cell.getStringCellValue());
						counterBuilderSecond.setFactoryNumberStr(cell.getStringCellValue());
						break;
					default:
						break;
					}  
					
					HSSFCell cellIndicationsDay = row.getCell(6);
					counterBuilder.setIndications(getVal(cellIndicationsDay, 0));
					counterBuilder.setTarrifZone(1.0);
					
					
					HSSFCell cellIndicationsNight = row.getCell(7);
					if (cellIndicationsNight.getCellType() == Cell.CELL_TYPE_BLANK){
						isBalnkNight = true;
					}
					counterBuilderSecond.setIndications(getVal(cellIndicationsNight, 0));
					counterBuilderSecond.setTarrifZone(2.0);
					iFiller.insert(counters, counterBuilder.build());
					if (!isBalnkNight){
						iFiller.insert(counters, counterBuilderSecond.build());
					}
					 
				}
			}

	 
	}

}
