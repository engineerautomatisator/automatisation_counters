package logic.interfaces;

import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.usermodel.CellStyle;

 

public interface IHelperHandlerRow {
	boolean trySetValueData(HSSFRow row, double num, CellStyle style);
	String getNumUcheta(HSSFRow row);
	double getTarif(HSSFRow row);
	double getDataByString(List<ICounter> sourcesList, String numUcheta, double tarif);
}
