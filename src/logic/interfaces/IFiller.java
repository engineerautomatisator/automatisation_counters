package logic.interfaces;

import java.util.List;

public interface IFiller {
	void insert(List<ICounter> counters, ICounter pretendent);
}
