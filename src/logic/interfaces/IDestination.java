package logic.interfaces;

public interface IDestination {
	String getFullPath();
	String getFullPathForSave();
	Integer getStartRow();
	Integer getEndRow();
}
