package logic.interfaces;

import java.util.List;

public interface IGeneratorCounter {
	void generate(List<ICounter> counters, IFiller iFiller) throws Exception;
}
