package logic.interfaces;

public enum TariffZone {
	DAY,
	NIGHT
}
