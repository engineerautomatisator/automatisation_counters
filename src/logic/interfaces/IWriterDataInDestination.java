package logic.interfaces;

import java.util.List;

public interface IWriterDataInDestination {
	void write(List<ICounter> counters, List<IDestination> destinations) throws Exception;
}
