package logic.interfaces;

import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
 

public interface IHandlerRow {
	 Integer handleRow(HSSFRow row, HSSFSheet sheet,
			 Integer index, List<ICounter> counters);
	 
}
