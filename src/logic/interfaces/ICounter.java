package logic.interfaces;

public interface ICounter {
	String getFactoryNumberStr(); // ����� ���������
	double getFactoryNumberNum();
	Double getTariffZone(); // ���� ����
	double getIndcations(); // ���������
	boolean isFound();
	void onFound();
}
