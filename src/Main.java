import javax.swing.JDialog;
import javax.swing.JFrame;

 

import ui.Window;

public class Main {
 
	public static void main(String[] args) {
		 javax.swing.SwingUtilities.invokeLater(new Runnable() {
		        public void run() {
		            JFrame.setDefaultLookAndFeelDecorated(true);
		            JDialog.setDefaultLookAndFeelDecorated(true);
		            new Window().init();
		        }
		    });	

	}

}
