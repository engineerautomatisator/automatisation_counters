package ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeoutException;

import javax.swing.Box;
import javax.swing.BoxLayout; 
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import logic.impls.Destination;
import logic.impls.Filler;
import logic.impls.GeneratorFromTNS;
import logic.impls.GeneratorFromTac;
import logic.impls.GeneratorFromWork;
import logic.impls.GeneratorWithoutAscue;
import logic.interfaces.ICounter;
import logic.interfaces.IDestination;
import logic.interfaces.IGeneratorCounter;
import ui.bridge.TaskBuild;
import ui.controllers.LoaderData;

public class Window  {
	 private final static String COUNT_FROM_TAC = "count_from_tac";
     private List<ICounter> m_countersDataSources = new ArrayList<>();
     private IGeneratorCounter m_generator;
   
     private final String sourcesStrings[] = { "�� ����", "�� ���", "��� �����" };
     private final TypeSource types[] = { TypeSource.FROM_TAC, TypeSource.FROM_TNS, TypeSource.WITHOUT_ASCUE };
     private TypeSource m_selected = TypeSource.FROM_TAC;
     JComboBox<String> bookList = new JComboBox<>(sourcesStrings);
     JList<String> jlst = new JList(sourcesStrings);
     IInit m_loader = new LoaderData();
    
     private List<IDestination> destinations = new ArrayList<>();
	 public void init(){
		 
		//�������� ���� � ��������� ���������
         final JFrame window = new JFrame("������� ����");
         window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
     	
 	    JPanel panel = new JPanel();
 	    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
 	     
 	    panel.add(Box.createVerticalGlue());
 	
 	   
 	 
 	    JButton buttonFileJob = new JButton("���� � ������");
 	    buttonFileJob.setAlignmentX(JComponent.CENTER_ALIGNMENT);
 	    JTextField textFieldFromWorkStart = new JTextField();
 	    JTextField textFieldFromWorkEnd  = new JTextField();
 	    buttonFileJob.addActionListener(new ActionListener() {
 	    	private final static String PREFIX_PATH = "D:/job_zarech/ready_job/";
 	    	private String getFileName(final String path){
	 	   		Path p = Paths.get(path);
	 	   		String fileName = p.getFileName().toString();
	 	   		String[] arr = fileName.split("\\.");
	 	   		String val = PREFIX_PATH + arr[0] + "_ready" + ".xls";
	 	   		return val;
 	    	}
 	        public void actionPerformed(ActionEvent e) {
 	            JFileChooser fileopen = new JFileChooser();             
 	            int ret = fileopen.showDialog(null, "������� ����");                
 	            if (ret == JFileChooser.APPROVE_OPTION) {
 	                File file = fileopen.getSelectedFile();
 	               Integer valStart = null;
	                try {
	                	valStart= Integer.parseInt(textFieldFromWorkStart.getText());
	            	} catch (NumberFormatException exce) {
	            		JOptionPane.showMessageDialog(null, "�� ������� ������������ ����� ����� ������ ������ �� ���");
	            		return;
	            	}
	                
	                Integer valEnd = null;
	                try {
	                	valEnd = Integer.parseInt(textFieldFromWorkEnd.getText());
	            	} catch (NumberFormatException exce) {
	            		JOptionPane.showMessageDialog(null, "�� ������� ������������ ����� - ����� ��������� ������ �� ���");
	            		return;
	            	}
	                IGeneratorCounter generator = new GeneratorFromWork(file.getAbsolutePath(),
	                		valStart, valEnd);
	                try {
						generator.generate(m_countersDataSources, new Filler());
					} catch (Exception e1) {
						e1.printStackTrace();
						JOptionPane.showMessageDialog(null, e1.getLocalizedMessage());
					}
	                String pathFromJob = file.getAbsolutePath();
	                String pathForSave = getFileName(pathFromJob);
	                destinations.add(new Destination.BuilderDestination()
	                		.setEndRow(valEnd).setStartRow(valStart)
	                		.setFullPath(pathFromJob)
	                		.setPathForSave(pathForSave).build());
 	            }
 	        }
 	    });
  
 	    panel.add(buttonFileJob);
 	  
 	    panel.add(Box.createRigidArea(new Dimension(10, 10)));
 	    
 	    textFieldFromWorkStart.setBackground(Color.WHITE);
 	    textFieldFromWorkStart.setColumns(1);
 	  	textFieldFromWorkStart.setText("������� ����� ������ ������ � ������");
 	  
	    panel.add(textFieldFromWorkStart);
 	    
 	   panel.add(Box.createRigidArea(new Dimension(10, 10)));
 	   
 	  panel.add(Box.createRigidArea(new Dimension(10, 10)));
	    
 	 textFieldFromWorkEnd.setBackground(Color.WHITE);
 	textFieldFromWorkEnd.setColumns(1);
 	  textFieldFromWorkEnd.setText("������� ����� ��������� ������ � ������");
 	    
	    panel.add(textFieldFromWorkEnd); 
	   panel.add(Box.createRigidArea(new Dimension(10, 10)));
 	  
	    
 	    JButton buttonFileTac = new JButton("���� � �������");
 	   
 
 	  panel.add(bookList);
 	 bookList.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			int index = bookList.getSelectedIndex();
			if (index == -1){
				System.out.println("NEED SELECT INDEX");
			}else {
				System.out.println(" SELECT FROM " + sourcesStrings[index]);
				m_selected = types[index];
			}
			
		}
	});
  
 	    buttonFileTac.setAlignmentX(JComponent.CENTER_ALIGNMENT);
 	    JTextField textField = new JTextField();
 	    buttonFileTac.addActionListener(new ActionListener() {
 	        public void actionPerformed(ActionEvent e) {
 	            JFileChooser fileopen = new JFileChooser();             
 	            int ret = fileopen.showDialog(null, "������� ����");                
 	            if (ret == JFileChooser.APPROVE_OPTION) {
 	                File file = fileopen.getSelectedFile();
 	                String countFromTac = textField.getText();
 	                Integer val = null;
 	                try {
 	            	  val = Integer.parseInt(countFromTac);
 	            	} catch (NumberFormatException exce) {
 	            		JOptionPane.showMessageDialog(null, "�� ������� ������������ ����� �� ���");
 	            		return;
 	            	}
 	                try {
 	                	if (m_selected == TypeSource.FROM_TAC){
 	                		m_generator = new GeneratorFromTac(file.getAbsolutePath(), val);
 	                	}else if (m_selected == TypeSource.FROM_TNS){
 	                		m_generator = new GeneratorFromTNS(file.getAbsolutePath(), val);
 	                	}else {
 	                		m_generator = new GeneratorWithoutAscue(file.getAbsolutePath(), val);
 	                	}
	 	                 
	 	               m_generator.generate(m_countersDataSources, new Filler());
 	                }catch(Exception exc){
 	                	JOptionPane.showMessageDialog(null, exc.getLocalizedMessage());
 	                }
 	               
 	            }
 	        }
 	    });
 	
 	
 	    panel.add(buttonFileTac);
 	    
 	    panel.add(Box.createRigidArea(new Dimension(10, 10)));
 	    
	    textField.setBackground(Color.WHITE);
	    textField.setColumns(1);
	    textField.setText("������� ����� ��������� ������ �� ����");
	    textField.setName(COUNT_FROM_TAC);
	    panel.add(textField);
	    panel.add(Box.createRigidArea(new Dimension(10, 10)));
	    
	    JButton btnPrint = new JButton("������");
	    btnPrint.setAlignmentX(JComponent.CENTER_ALIGNMENT);
 	
	    btnPrint.addActionListener(new ActionListener() {
 	        public void actionPerformed(ActionEvent e) {
 	        	int i = 0;
 	        	System.out.println("COUNTERS : ");
 	            for (ICounter iCounter : m_countersDataSources){
 	            	System.out.println(i + " =i;  " + iCounter.getFactoryNumberNum() + " " +
 	            			iCounter.getFactoryNumberStr() + " " +
 	            			iCounter.getIndcations() + " " +
 	            			iCounter.getTariffZone() + " ");
 	            	++i;
 	            }
 	           i = 0;
 	            System.out.println("destinations ");
 	            for (IDestination dest : destinations){
 	            	System.out.println(dest.getFullPath() + " is full path " + 
 	            			dest.getFullPathForSave() + " for save");
 	            	++i;
 	            }
 	        }
 	    });
	    panel.add(btnPrint);
        panel.add(Box.createRigidArea(new Dimension(10, 10)));
 		
 
 	    
 panel.add(Box.createRigidArea(new Dimension(10, 10)));
 		
 	    JButton run = new JButton("���������!");
 	    run.setAlignmentX(JComponent.CENTER_ALIGNMENT);
 	
 	    run.addActionListener(new ActionListener() {
 	        public void actionPerformed(ActionEvent e) {
 	        	ExecutorService executor = Executors.newFixedThreadPool(1);
 	        	Callable<Boolean> callable = new TaskBuild(m_countersDataSources, destinations);
 	        	FutureTask<Boolean> futureTask1 = new FutureTask<Boolean>(callable);
 	        	executor.execute(futureTask1);
 	        	final String PREFIX_TEXT = "�������������� �����";
 	        	run.setText(PREFIX_TEXT);
 	        	int i = 0;
 	        	while (true){
 	        		try {
						Thread.sleep(100);
					} catch (InterruptedException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
        			if (futureTask1.isDone()){
        				try {
							futureTask1.get();
						} catch (InterruptedException | ExecutionException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
							JOptionPane.showMessageDialog(null, e1.getLocalizedMessage());
						}
        				run.setText("������!");
        				 executor.shutdown();
                         return;
        			}
        			++i;
        			i = i % 4;
        			StringBuilder stringBuilder = new StringBuilder();
        			for (int j = 0; j < i; ++j){
        				stringBuilder.append(".");
        			}
        			run.setText(PREFIX_TEXT + stringBuilder.toString());
        			
 	        	}
 	        }
 	    });
 	
 	    panel.add(run);
 	    
 	
 	  panel.add(Box.createVerticalGlue());
 	   window.getContentPane().add(panel);
 	
 	  window.setPreferredSize(new Dimension(960,640));
 	  window.pack();
 	  window.setLocationRelativeTo(null);
 	  window.setVisible(true);
	 }
	 
	 public Window(){ 
		
	 }
     
}
