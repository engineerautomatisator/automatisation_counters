package ui;

import java.awt.Panel;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

public interface IInit extends ActionListener {
	void init(JPanel panel);
}
