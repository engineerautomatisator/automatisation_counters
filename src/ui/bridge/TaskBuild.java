package ui.bridge;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import logic.impls.WriterDataInDestination;
import logic.interfaces.ICounter;
import logic.interfaces.IDestination;
import logic.interfaces.IWriterDataInDestination;

public class TaskBuild implements Callable<Boolean> {

	private List<ICounter> m_listCounters = new ArrayList<>();
	private List<IDestination> m_destinations = new ArrayList<>();
	private IWriterDataInDestination m_writer;
	public TaskBuild(List<ICounter> _listCounters, List<IDestination> _destinations){
		m_listCounters = _listCounters;
		
		m_destinations = _destinations;
		m_writer = new WriterDataInDestination();
	}
	@Override
	public Boolean call() throws Exception {
		m_writer.write(m_listCounters, m_destinations);
		
		return Boolean.TRUE;
	}

}
