package ui.controllers;

import java.awt.Color;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

import ui.IInit;

public class LoaderData implements IInit{

	
	private boolean m_isVisible = true;
 
	private JPanel m_innerPanel;
	private JButton m_btnFileChoose;
	private JButton m_btnLoad;
	private JTextField m_textField;
	public LoaderData(){
	 
		m_innerPanel = new JPanel();
		m_btnFileChoose = new JButton("������� ����");
		m_btnLoad = new JButton("��������� ������");
		m_textField = new JTextField();
		m_textField.setBackground(Color.WHITE);
		m_textField.setColumns(14);
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if (m_isVisible){
			m_isVisible = false;
		} else {
			m_isVisible = true;
		}
		m_innerPanel.setVisible(m_isVisible);
	}
	
	@Override
	public void init(JPanel panel) {
		m_innerPanel.add(m_textField);
		m_innerPanel.add(m_btnLoad);
		m_innerPanel.add(m_btnFileChoose);
		panel.add(m_innerPanel);
		m_innerPanel.setVisible(true);
		
	}

}
